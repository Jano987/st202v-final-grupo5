/*
 * disminuirUnidad.cpp
 *
 *  Created on: 20 jun. 2018
 *      Author: jano_
 */
#include <iostream>
#include <string>

#include <fstream>
#include"../utilitarios/utilitarios.hpp"
using namespace std;

struct inventario{
	string producto;
	string fecha;
	float precio;
	int cantidad;

	void inicializar(vector<string> datos ){
		producto=datos[0];
		fecha=datos[1];
		precio=convertirAreal(datos[2]);
		cantidad=convertirAentero(datos[3]);
	}
};

struct publicacion{
	string area;
	string nombre;
	string nombreUser;
	string fecha;
	int estado;
	float precio;
	int cantidad;
	string descripcion;

	void inicializar(vector<string> datos){
		area=datos[0];
		nombre=datos[1];
		nombreUser=datos[2];
		fecha=datos[3];
		estado=convertirAentero(datos[4]);
		precio=convertirAreal(datos[5]);
		cantidad=convertirAentero(datos[6]);
		descripcion=datos[7];
	}

};

void eliminarPubl(string nombUser,string nombP){

	vector<string> auxI;
	vector<string> auxP;

	ifstream archivo;
	string linea,ruta1,nombre;

	ruta1="Archivos/Vendedor/Inventario/Inventario_";
	nombre=ruta1.append(nombUser).append(".txt");
	archivo.open(nombre.c_str());

	bool salir=false;
	int ubicacionPubli=0;
	if(archivo.is_open()){
		while(getline(archivo,linea)){
			if(linea.find(nombP)!=string::npos){
				salir=true;
			}
			if(salir==false){
				ubicacionPubli++;
			}
			auxI.push_back(linea);
		}
	}

	archivo.close();

	string op;
	ofstream archivo1;
	if(salir==true){
		auxI.erase(auxI.begin()+ubicacionPubli);
		archivo1.open(nombre.c_str(),ios::trunc);
		for(int i=0;i<auxI.size();i++){
			archivo1<<auxI[i]<<endl;
		}
		archivo1.close();
	}


	archivo.open("Archivos/Publicaciones.txt");

	salir=false;
	ubicacionPubli=0;

	if(archivo.is_open()){
		while(getline(archivo,linea)){
			if(linea.find(nombP)!=string::npos){
				salir=true;
			}
			if(salir==false){
				ubicacionPubli++;
			}
			auxP.push_back(linea);
		}
	}
	archivo.close();

	if(salir==true){
		auxP.erase(auxP.begin()+ubicacionPubli);
		archivo1.open("Archivos/Publicaciones.txt",ios::trunc);
		for(int i=0;i<auxP.size();i++){
			archivo1<<auxP[i]<<endl;
		}
		archivo1.close();

	}

}


void disminuirInvPubl(string nombreDeUsuario, string nombProd){

	ifstream archivo;
	string linea;
	string ruta,nombre;
	ruta="Archivos/Vendedor/Inventario/Inventario_";
	nombre=ruta.append(nombreDeUsuario).append(".txt");

	vector<inventario> I;
	inventario otherI;
	vector<string> datos;

	archivo.open(nombre.c_str());
	while(getline(archivo,linea)){

			datos=separacion(linea,'|');
			otherI.inicializar(datos);
			I.push_back(otherI);
	}
	archivo.close();
	//Ya tengo almacenado el I (struct inventario)

	vector<publicacion> P;
	publicacion otherP;
	vector<string> datos1;

	archivo.open("Archivos/Publicaciones.txt");
	while(getline(archivo,linea)){
		datos1=separacion(linea,'|');
		otherP.inicializar(datos1);
		P.push_back(otherP);
	}
	archivo.close();
	//Ya esta almacenado el P (struct publicacion)

	ofstream archivo2;
	for(int i=0;i<I.size();i++){
		if(I[i].producto==nombProd){
			if(I[i].cantidad>1){

				I[i].cantidad--;//se disminuye en una unidad

				archivo2.open(nombre.c_str(),ios::trunc);

				for(int i=0;i<I.size();i++){
					archivo2<<I[i].producto<<'|'<<I[i].fecha<<'|'<<I[i].precio<<'|'<<I[i].cantidad<<endl;
				}

				archivo2.close();
				//Ya se modifico el Inventario respecto a la cantidad del producto mencionado



			}else{
				eliminarPubl(nombreDeUsuario,nombProd);
			}

		}
	}

	for(int i=0;i<P.size();i++){
		if(P[i].nombre==nombProd){
			if(P[i].cantidad>1){

				P[i].cantidad--;//Se disminuye en una unidad

				archivo2.open("Archivos/Publicaciones.txt",ios::trunc);

				for(int i=0;i<P.size();i++){
					archivo2<<P[i].area<<'|'<<P[i].nombre<<'|'<<P[i].nombreUser<<'|'<<P[i].fecha<<'|'<<P[i].estado<<'|'<<P[i].precio<<'|'<<P[i].cantidad<<endl;
				}
				archivo2.close();

			}else{
				eliminarPubl(nombreDeUsuario,nombProd);
			}
		}
	}

}


