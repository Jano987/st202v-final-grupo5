/*
 * publicar.hpp
 *
 *  Created on: 22 may. 2018
 *      Author: jano_
 */

#ifndef PUBLICAR_PUBLICAR_HPP_
#define PUBLICAR_PUBLICAR_HPP_

#include <iostream>
#include <vector>
#include "../utilitarios/utilitarios.hpp"

using namespace std;

struct publicar{
	string area;
	int num_ciclo;
	string nombre;
	string nombreUser;
	float precio;
	string descripcion;
	int estado;
	int cantidad;

};

void Publicar(string nombredeusuario);


#endif /* PUBLICAR_PUBLICAR_HPP_ */
