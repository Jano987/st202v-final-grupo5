/*
 * publicar.cpp
 *
 *  Created on: 02 jun. 2018
 */

#include <vector>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string>
#include "../ingreso/ingreso.hpp"
#include "../utilitarios/utilitarios.hpp"
#include "publicar.hpp"
#include <fstream>
using namespace std;



void Publicar(string nombredeusuario){
	int cat1,cat2,can,ciclo,pr,estad;
	char publ,area,c;
	string ruta,des, producto;
	cout<<"Ingrese una de las siguientes categorias: "<<endl;
	cout<<"<1> Libros "<<endl;
	cout<<"<2> Indumentaria"<<endl;
	cout<<"<3> Instrumentos"<<endl;
	cout<<"<4> Articulos tecnologicos"<<endl;
	cin>>cat1;

	cout<<"Ingrese el area a la que pertenece su producto: "<<endl;
	cout<<"<1> Ciencias de la salud "<<endl;
	cout<<"<2> Ingenieria"<<endl;
	cout<<"<3> Ciencias basicas"<<endl;
	cout<<"<4> Ciencias de la administracion"<<endl;
	cout<<"<5> Ciencias sociales"<<endl;
	cin>>cat2;
	fflush(stdout);
	fflush(stdin);

	switch(cat2){
	case 1: area='a';
		break;
	case 2: area='b';
		break;
	case 3: area='c';
		break;
	case 4: area='d';
		break;
	case 5: area='e';
		break;
	}

	cout<<"Ingrese el ciclo para este articulo: "<<endl;
	cin>>ciclo;
	fflush(stdout);
	fflush(stdin);
	cout<<"Nombre del producto: "<<endl;
	getline(cin,producto);
	fflush(stdout);
		fflush(stdin);
	cout<<"Precio del producto: "<<endl;
	cin>>pr;
	fflush(stdout);
		fflush(stdin);
	cout<<"Cantidad de productos: "<<endl;
	cin>>can;
	fflush(stdout);
		fflush(stdin);
	cout<<"Estado del producto(1-4 Mal estado,5-7 Seminuevo,7-10 Nuevo): "<<endl;
	cin>>estad;
	fflush(stdout);
	fflush(stdin);
	cout<<"Descripción del articulo: "<<endl;
	getline(cin,des);
	fflush(stdout);
		fflush(stdin);
	cout<<"Presione p para publicar: "<<endl;
	cin>>publ;
	fflush(stdout);
		fflush(stdin);
	if(publ=='p'){
		ruta="Archivos/Vendedor/Inventario/Inventario_";
		string fech;
		string nombre;
		fech=obtenerfecha();
		nombre=ruta.append(nombredeusuario).append(".txt");
		ofstream archivo;
		archivo.open(nombre.c_str(),ios::app);
		archivo<<producto<<'|'<<fech<<'|'<<pr<<'|'<<can<<endl;

		archivo.close();

		archivo.open("Archivos/Publicaciones.txt",ios::app);
		archivo<<cat1<<area<<ciclo<<'|'<<producto<<'|'<<nombredeusuario<<'|'<<fech<<'|'<<estad<<'|'<<pr<<'|'<<can<<'|'<<des<<endl;
		archivo.close();
	}

	do{
		cout<<"Presione x para volver"<<endl;
		cin>>c;
	if(c=='x'){
		IngresoVendedor(nombredeusuario);
	}
	}while(c!='x');

}
