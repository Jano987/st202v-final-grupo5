/*
 * entregaspendientes.cpp
 *
 *  Created on: 19 jun. 2018
 *      Author: Jose
 */
#include <iostream>
#include <fstream>
#include <string>
#include "../ingreso/ingreso.hpp"
#include "../utilitarios/utilitarios.hpp"
using namespace std;
void entregas(string nombredeusuario){
	string ruta;
	string linea;
	string nombre;

	ruta="Archivos/Vendedor/Entrega/Entregas_";
	nombre=ruta.append(nombredeusuario).append(".txt");

	ifstream archivo;
	archivo.open(nombre.c_str(),ios::in);
	while(getline(archivo,linea)){
		separacionYmuestraDeEntregas(linea,'|');
	}

	archivo.close();
	cout<<"<1>Volver"<<endl;
	int op;
	cin>>op;
	if(op==1){
		IngresoVendedor(nombredeusuario);
	}
}



