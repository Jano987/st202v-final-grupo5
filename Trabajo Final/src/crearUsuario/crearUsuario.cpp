/*
 * crearUsuario.cpp
 *
 *  Created on: 22 may. 2018
 *      Author: jano_
 */

#include <iostream>

#include <vector>
#include<stdio.h>
#include<string>
#include<sstream>
#include "../bienvenida/bienvenida.hpp"
#include"fstream"
#include "../inicioSesion/inicioSesion.hpp"
#include "../utilitarios/utilitarios.hpp"

using namespace std;

string nombCom,email,nombUser,clave,claveAgain,fechaNac;
int dia,mes,anio;
char sexo;

struct Comprador{
	string nombreCompleto;
	string nombreUsuario;
	char sexo;
	string fechaNacimiento;
	string email;
	string clave;


	string obtenerFecha(int d,int m,int a){
		stringstream fecha;

		fecha<<d<<" / "<<m<<" / "<<a;
		return fecha.str();
	}



	void Almacenar(string nC,string nU,char s,string em,int d,int m,int a,string cl){
		stringstream Convertir;
		nombreCompleto=nC;
		nombreUsuario=nU;
		sexo=s;
		fechaNacimiento=obtenerFecha(d,m,a);
		email=em;
		clave=cl;
	}


};

struct Vendedor{
	string nombreCompleto;
	string nombreUsuario;
	char sexo;
	string fechaNacimiento;
	string email;
	string clave;
	string DNI;
	string empresa;
	string RUC;


	string obtenerFecha(int d,int m,int a){
		stringstream fecha;

		fecha<<d<<" / "<<m<<" / "<<a;
		return fecha.str();
	}



	void Almacenar(string nC,string nU,char s,string em,int d,int m,int a,string cl,string dni,string emp,string r){
		stringstream Convertir;
		nombreCompleto=nC;
		nombreUsuario=nU;
		sexo=s;
		fechaNacimiento=obtenerFecha(d,m,a);
		email=em;
		clave=cl;
		DNI=dni;
		empresa=emp;
		RUC=r;

	}


};

void Registro_Vendedor();
void Registro_Comprador();

typedef struct Comprador Comprador;
typedef struct Vendedor Vendedor;

void EleccionUsuario(){
	int opelecusu1;
	cout<<"=================================="<<endl;
	//Elegir tipo de usuario al registrarse
	cout<<"<1> Registrarse como usuario vendedor"<<endl;
	cout<<"    Presione <1> para continuar."<<endl;
	cout<<endl;
	cout<<"<2> Registrarse como usuario comprador"<<endl;
	cout<<"    Presione <2> para continuar."<<endl;
	cout<<endl;
	cout<<"<0> Menu"<<endl;
	cout<<"=================================="<<endl;
	cin>>opelecusu1;
	if(opelecusu1==0){
		Menu_Principal();
	}
	if(opelecusu1==1){
		Registro_Vendedor();
	}
	if(opelecusu1==2){

		Registro_Comprador();

	}


}

void Crear_Inventario(string nombreusuario){
	ofstream archivo;
			string nombre,dato,usuario;
			usuario=nombreusuario;
			dato="Archivos/Vendedor/Inventario/Inventario_";
			nombre=dato.append(usuario).append(".txt");
			archivo.open(nombre.c_str(),ios::out);
			archivo.close();
			//el nombre del archivo se debe guardar tambien
}
void Crear_Historial(string nombreusuario){
	ofstream archivo;
			string nombre,dato,usuario;
			usuario=nombreusuario;
			dato="Archivos/Comprador/Historial/Historial_";
			nombre=dato.append(usuario).append(".txt");
			archivo.open(nombre.c_str(),ios::out);
			archivo.close();
			//el nombre del archivo se debe guardar tambien
}
void Crear_Pedidos(string nombreusuario){
	ofstream archivo;
			string nombre,dato,usuario;
			usuario=nombreusuario;
			dato="Archivos/Comprador/Pedido/Pedidos_";
			nombre=dato.append(usuario).append(".txt");
			archivo.open(nombre.c_str(),ios::out);
			archivo.close();
			//el nombre del archivo se debe guardar tambien
}

void Crear_Entregas(string nombreusuario){
	ofstream archivo;
	 string nombre,dato,usuario;
	 usuario=nombreusuario;
	 dato="Archivos/Vendedor/Entrega/Entregas_";
	 			nombre=dato.append(usuario).append(".txt");
	 			archivo.open(nombre.c_str(),ios::out);
	 			archivo.close();
}
void Registro_Vendedor(){
	int opvendedor;
	string DNI,empresa,RUC;
	Vendedor vend;
	ofstream archivo;

	cout<<"=================================="<<endl;
	cout<<"\t~�Registrate y vende ya!~"<<endl;
	cout<<endl;
	fflush(stdout);
	cout<<"* Nombres completos: "<<endl;
	fflush(stdin);
	getline(cin,nombCom);
	cout<<"* Nombre de Usuario: "<<endl;
	getline(cin,nombUser);
	cout<<"* Sexo (M/F) : "<<endl;
	fflush(stdout);
	cin>>sexo;
	fflush(stdin);

	cout<<"* Fecha de Nacimiento: "<<endl;
	cout<<"Dia => ";
	cin>>dia;
	cout<<"Mes => ";
	cin>>mes;
	cout<<"Anio => ";
	cin>>anio;
	cout<<endl;
	fflush(stdout);
	cout<<dia<<" / "<<mes<<" / "<<anio<<endl<<endl;
	cout<<"* E-mail: "<<endl;
	fflush(stdin);
	getline(cin,email);
	fflush(stdout);

	cout<<"* DNI: "<<endl;
	fflush(stdin);
	getline(cin,DNI);
	fflush(stdout);
	cout<<"** Registre su empresa (opcional)"<<endl;
	cout<<"* Empresa: "<<endl;
	fflush(stdin);
	getline(cin,empresa);
	cout<<"* RUC: "<<endl;
	fflush(stdout);
	getline(cin,RUC);

	bool iguales=false;
	while(iguales==false){
			cout<<"* Clave(Como minimo 6 caracteres): "<<endl;
			cin>>clave;
			cout<<"* Vuelva a escribir su clave: "<<endl;
			cin>>claveAgain;
			if(clave==claveAgain){
				iguales=true;
			}else{
			cout<<"\nClaves diferentes, por favor rellene de nuevo\n\n";
			}
		}
	//Utilizando cifrado de Cesar, numero de espacios que se utilizan para cambiar un caracter: n = 6.
	clave=codigoCesar(clave,6);

	cout<<endl;
	cout<<"Presione 2 para confirmar"<<endl;
	fflush(stdout);
	int ops;
	cin>>ops;

	if(ops==2){
		Crear_Inventario(nombUser);//crea un archivo-inventario para cada usuario
		Crear_Entregas(nombUser);
		archivo.open("Archivos/Informacion/InfoUsuariosVendedor.txt",ios::app);
		vend.Almacenar(nombCom,nombUser,sexo,email,dia,mes,anio,clave,DNI,empresa,RUC);
		archivo<<vend.nombreUsuario<<'|'<<vend.nombreCompleto<<'|'<<vend.sexo<<'|'<<vend.fechaNacimiento<<'|'<<vend.email<<'|'<<vend.clave<<'|'<<vend.DNI<<'|'<<vend.empresa<<'|'<<vend.RUC<<'|'<<endl;
		archivo.close();

	cout<<"<1> �Ya tienes una cuenta? Inicia Sesion."<<endl;
	//Me redirige a la pantalla Iniciar sesion
	//Pantalla especifica: Inicio_Sesion
	cout<<endl;
	cout<<"<0> Menu"<<endl;
	cout<<endl;
	cout<<"=================================="<<endl;
	cin>>opvendedor;
	if(opvendedor==0){
		Menu_Principal();
	}
	if(opvendedor==1){
		Inicio_Sesion();
	}
}
}



void Registro_Comprador(){
	int opcomprador;
	Comprador comp;


	ofstream archivo;

	cout<<"=================================="<<endl;
	cout<<"\t~�Registrate y compra ya!~"<<endl;
	cout<<endl;
	fflush(stdout);
	cout<<"* Nombres completos: "<<endl;
	fflush(stdin);
	getline(cin,nombCom);
	fflush(stdout);
	cout<<"* Nombre de Usuario: "<<endl;
	fflush(stdin);
	getline(cin,nombUser);
	cout<<"* Sexo (M/F) : "<<endl;
	cin>>sexo;
	cout<<"* Fecha de Nacimiento: "<<endl;
	cout<<"Dia => ";cin>>dia;cout<<"Mes => ";cin>>mes;cout<<"Anio => ";cin>>anio;cout<<endl;
	cout<<dia<<" / "<<mes<<" / "<<anio<<endl<<endl;

	fflush(stdout);
	cout<<"* E-mail: "<<endl;
	fflush(stdin);
	getline(cin,email);
	bool iguales=false;
	while(iguales==false){
		cout<<"* Clave(Como minimo 6 caracteres): "<<endl;
		cin>>clave;
		cout<<"* Vuelva a escribir su clave: "<<endl;
		cin>>claveAgain;
		if(clave==claveAgain){
			iguales=true;
		}else{
		cout<<"\nClaves diferentes, por favor rellene de nuevo\n\n";
		}
	}

	//Utilizando cifrado de Cesar, numero de espacios que se utilizan para cambiar un caracter: n = 6.
	clave=codigoCesar(clave,6);


	cout<<endl;
	cout<<"Presione 2 para confirmar"<<endl;
	fflush(stdout);
	int ops;
	cin>>ops;
	if(ops==2){
		Crear_Historial(nombUser);
		Crear_Pedidos(nombUser);
		archivo.open("Archivos/Informacion/InfoUsuariosComprador.txt",ios::app);
		comp.Almacenar(nombCom,nombUser,sexo, email,dia,mes,anio,clave);
		archivo<<comp.nombreUsuario<<'|'<<comp.nombreCompleto<<'|'<<comp.sexo<<'|'<<comp.fechaNacimiento<<'|'<<comp.email<<'|'<<comp.clave<<'|'<<endl;
		archivo.close();
	}

	cout<<"<1> �Ya tienes una cuenta? Inicia Sesion."<<endl;
	//Me redirige a la pantalla Iniciar sesion
	//Pantalla especifica: Inicio_Sesion
	cout<<endl;
	cout<<"<0> Menu"<<endl;
	cout<<endl;
	cout<<"=================================="<<endl;
   cin>>opcomprador;
	if(opcomprador==0){
		Menu_Principal();
	}
	if(opcomprador==1){
		Inicio_Sesion();
	}
}
