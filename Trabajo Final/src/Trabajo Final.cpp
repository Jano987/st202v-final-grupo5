//============================================================================
// Name        : Trabajo.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdlib.h>
#include "bienvenida/bienvenida.hpp"
#include "busqueda/busqueda.hpp"
#include "crearUsuario/crearUsuario.hpp"
#include "disminuirUnidad/disminuirUnidad.hpp"
#include "eliminarpublicacion/eliminarpublicacion.hpp"
#include "entregaspendientes/entregaspendientes.hpp"
#include "ganancias/ganancias.hpp"
#include "historial/historial.hpp"
#include "ingreso/ingreso.hpp"
#include "inicioSesion/inicioSesion.hpp"
#include "pedidospendientes/pedidospendientes.hpp"
#include "perfil/perfil.hpp"
#include "publicar/publicar.hpp"
#include "revisarinventario/revisarinventario.hpp"
#include "utilitarios/utilitarios.hpp"
#include "ofertas/ofertas.hpp"

using namespace std;

int main() {

	Bienvenida();

	return 0;
}
