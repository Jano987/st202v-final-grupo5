/*
 * ingreso.cpp
 *
 *  Created on: 22 may. 2018
 *      Author: jano_
 */

#include <iostream>
#include <stdio.h>
#include <string>
#include "../busqueda/busqueda.hpp"
#include "../bienvenida//bienvenida.hpp"
#include "../perfil/perfil.hpp"
#include "../publicar/publicar.hpp"
#include "../revisarinventario/revisarinventario.hpp"
#include "../eliminarpublicacion/eliminarpublicacion.hpp"
#include "../entregaspendientes/entregaspendientes.hpp"
#include "../pedidospendientes/pedidospendientes.hpp"
#include "../historial/historial.hpp"
using namespace std;

int num;

void IngresoComprador(string nombredeusuario){
	cout<<"=================================="<<endl<<endl;
	cout<<"�Bienvenido universitario! "<<endl<<endl;
	cout<<"�Que es lo que desea hacer?"<<endl<<endl;
	cout<<"<1> Buscar un producto universitario: "<<endl;
	cout<<"<2> Ver su perfil: "<<endl;
	cout<<"<3> Ver su historial de compras: "<<endl;
	/*la 4 y 5 hacen ver el contenido del archivo, en historial solo faltaria como se edita el archivo al comprar
	 *y en carrito igual como se edita y la logica de confirmar el pedido y eso como afecta al inventario del vendedor
	 */
	cout<<"<4> Ver sus pedidos pendientes"<<endl;
    cout<<"<5> Salir"<<endl;
	cout<<"=================================="<<endl;
	cin>>num;
	if(num==1){
		Busqueda(nombredeusuario);
	}
	if(num==2){
		PerfilComprador(nombredeusuario);
	}
	if(num==3){
		historial(nombredeusuario);
	}
	if(num==4){
		pedidos(nombredeusuario);
	}
	if(num==5){
		Menu_Principal();
	}


}
void IngresoVendedor(string nombredeusuario){
	cout<<"=================================="<<endl<<endl;
	cout<<"      �Bienvenido a EDUSHOP! "<<endl<<endl;
	cout<<"�Que es lo que desea hacer?"<<endl<<endl;
	cout<<"<1> Publicar su producto universitario"<<endl;
	cout<<"<2> Ver su perfil"<<endl;
	cout<<"<3> Ver su inventario"<<endl;
	//cout<<"<4> Ver sus ganancias hasta la fecha"<<endl;
	cout<<"<4> Eliminar publicacion"<<endl;
	cout<<"<5> Ver entregas pendientes"<<endl;
	cout<<"<6> Salir"<<endl;
	cout<<"=================================="<<endl;
	cin>>num;
	if(num==4){
		string nombredepublicacion;
		cout<<"Introduzca el nombre de su publicacion:"<<endl;
		fflush(stdout);
		fflush(stdin);
		getline(cin,nombredepublicacion);
	 eliminarpublicacion(nombredeusuario,nombredepublicacion);
	}
	if(num==1){
		Publicar(nombredeusuario);
	}

	if(num==2){
		PerfilVendedor(nombredeusuario);
	}
	if(num==3){
		revisar(nombredeusuario);
	}

    if(num==5){
   	 entregas(nombredeusuario);
    }
	if(num==6){
		Menu_Principal();
	}


}
