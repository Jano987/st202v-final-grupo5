/*
 * busqueda.cpp
 *
 *  Created on: 22 may. 2018
 *      Author: jano_
 */

#include <iostream>
#include <string>
#include <vector>
#include "../ingreso/ingreso.hpp"
#include "../utilitarios/utilitarios.hpp"
#include "../disminuirUnidad/disminuirUnidad.hpp"
#include "busqueda.hpp"
#include<stdio.h>
#include<fstream>

using namespace std;

struct publicar{
	string area;
	string nombre;
	string nombreUser;
	string fecha;
	int estado;
	float precio;
	int cantidad;
	string descripcion;


	void inicializar(vector<string> datos){
		area=datos[0];
		nombre=datos[1];
		nombreUser=datos[2];
		fecha=datos[3];
		estado=convertirAentero(datos[4]);
		precio=convertirAreal(datos[5]);
		cantidad=convertirAentero(datos[6]);
		descripcion=datos[7];
	}

	void mostrar(){
		cout<<"\nNombre de producto: "<<nombre<<endl;
		cout<<"Nombre del vendedor "<<nombreUser<<endl;
	}

	void mostrar2(){
		cout<<"\nArea: "<<area<<endl;
		cout<<"Nombre de producto: "<<nombre<<endl;
		cout<<"Nombre del vendedor "<<nombreUser<<endl;
		cout<<"Fecha "<<fecha<<endl;
		cout<<"Estado: "<<estado<<endl;
		cout<<"Precio: "<<precio<<endl;
		cout<<"Cantidad: "<<cantidad<<endl;
		cout<<"Descripcion: "<<descripcion<<endl;

	}

};

void Busqueda(string nombredeusuario){
	int cat2;
	string letracat,cat1,numciclo,codigo;
	cout<<"=================================="<<endl;
	cout<<"\t~�Todo lo que necesitas para la U en un solo lugar!~"<<endl;
	int d;

	vector<publicar> carrito;
	do{
		cout<<"> Buscar productos por: "<<endl;
		cout<<"Ingrese la categoria a la que pertenece el articulo que busca: "<<endl;
		cout<<"<1> Libros "<<endl;
		cout<<"<2> Indumentaria"<<endl;
		cout<<"<3> Instrumentos"<<endl;
		cout<<"<4> Articulos tecnologicos"<<endl;
		fflush(stdout);
		fflush(stdin);
		getline(cin,cat1);
		cout<<"Ingrese el area a la que pertenece el articulo que desea buscar "<<endl;
		cout<<"<1> Ciencias de la salud "<<endl;
		cout<<"<2> Ingenieria"<<endl;
		cout<<"<3> Ciencias basicas"<<endl;
		cout<<"<4> Ciencias de la administracion"<<endl;
		cout<<"<5> Ciencias de sociales"<<endl;
		fflush(stdout);
		fflush(stdin);
		cin>>cat2;
		switch(cat2){
			case 1: letracat="a";
			break;
			case 2: letracat="b";
			break;
			case 3: letracat="c";
			break;
			case 4: letracat="d";
			break;
			case 5: letracat="e";
			break;
		};
		cout<<"Ingrese el ciclo academico del articulo que desea buscar: "<<endl;
		fflush(stdout);
		fflush(stdin);
		getline(cin,numciclo);

		codigo=cat1.append(letracat).append(numciclo);//el codigo que se usara para buscar
		ifstream archivo;
		archivo.open("Archivos/Publicaciones.txt",ios::in);

		vector<publicar> p; //Almacena cada struct sobre publicacion en un vector<struct publicar>
		publicar otherP;
		vector<string> datos;
		string linea;
		int i=1;

		while(getline(archivo,linea)){
			if(linea.find(codigo)!=string::npos){
				datos=separacion(linea,'|');
				otherP.inicializar(datos);
				p.push_back(otherP);//Se almacena la publicacion como struct
				cout<<"Numero de producto: "<<i<<endl;
				separacionYmuestraDePublicacion(linea,'|');//Se muestra la publicacion en pantalla
				i++;
			}
		}
		archivo.close();

		//Ahora usamos el vector p para que el usuario elija el producto que desea comprar
		cout<<"\n�Desea a�adir un producto al carrito de compras? (S/N) "<<endl;
		char a,c,f;
		int e;
		cin>>a;

		/* Este codigo era SOLO para ver el error en el vector publicar
		cout<<"Mostrando vector<publicar> p"<<endl<<endl;
		for(int i=0;i<p.size();i++){
			p[i].mostrar2();
		}
		*/

		if(a=='s'){

				int j;
				cout<<"\nEscriba el numero de producto: ";
				cin>>j;
				carrito.push_back(p[j-1]);

				cout<<"\n�Desea ver su carrito? (S/N) ";
				cin>>c;
				cout<<endl;
				if(c=='s'){
					for(int i=0;i<carrito.size();i++){
						cout<<"Producto N�"<<i+1<<endl;
						carrito[i].mostrar();
						cout<<endl;
					}

					cout<<"\nElija una opcion para el carrito: "<<endl;
					cout<<"<1> Eliminar un producto."<<endl;
					cout<<"<2> A�adir otro producto."<<endl;
					cout<<"<3> Comprar los productos."<<endl;
					cin>>d;
					if(d==1){
						do{
							cout<<"\nEscriba el numero del producto que desea eliminar: ";
							cin>>e;
							carrito.erase(carrito.begin()+e-1);
							cout<<"\nResultado"<<endl;
							for(int i=0;i<carrito.size();i++){
								cout<<"Producto N�"<<i+1<<endl;
								carrito[i].mostrar();
								cout<<endl;
							}
							cout<<"\n�Desea eliminar otro producto? (S/N) ";
							cin>>f;
						}while(f=='s');
					}
					if(d==3){
						/*Cuando va a comprar los productos se le informara los productos al
						 * vendedor en sus ganancias y ademas se le eliminar� de su inventario
						 */
						string tarjeta;

						bool salir=false;
						do{
							cout<<"Ingrese su numero de tarjeta de credito: ";
							fflush(stdout);
							fflush(stdin);

							getline(cin,tarjeta);

							archivo.open("Archivos/numeroTarjetasVerdaderas.txt");
							cout<<"\nCARGANDO...."<<endl<<endl;
							while(getline(archivo,linea)){
								if(linea.find(tarjeta)!=string::npos && tarjeta.length()==19 ){
									cargando();

									string fechaCompra=obtenerfecha();
									string direccion;
									cout<<"\nIngrese la direccion a la cual llegara el pedido"<<endl;
									getline(cin,direccion);
									ofstream archivo1;
									string nombre,usuario,ruta;
									ruta="Archivos/Comprador/Pedido/Pedidos_";
									usuario=nombredeusuario;
									nombre=ruta.append(usuario).append(".txt");
									archivo1.open(nombre.c_str(),ios::app);
									for(int i=0;i<carrito.size();i++){
										archivo1<<carrito[i].nombre<<'|'<<carrito[i].nombreUser<<'|'<<fechaCompra<<'|'<<carrito[i].estado<<'|'<<carrito[i].precio<<'|'<<carrito[i].cantidad<<'|'<<carrito[i].descripcion<<endl;
									}
									archivo1.close();

									//Se le avisa a cada vendedor en sus entregas pendientes
									for(int i=0;i<carrito.size();i++){
										ofstream archivo2;
										string nombre2,usuario2,ruta2;
										usuario2=carrito[i].nombreUser;
										ruta2="Archivos/Vendedor/Entrega/Entregas_";
										nombre2=ruta2.append(usuario2).append(".txt");
										archivo2.open(nombre2.c_str(),ios::app);
										archivo2<<carrito[i].nombre<<"|"<<nombredeusuario<<'|'<<direccion<<endl;
										archivo2.close();
									}

									/*Tanto en el Inventario como en Publicaciones el producto
									debe disminuir su cantidad en una unidad
									*/
									string produc;
									for(int i=0;i<carrito.size();i++){
										usuario=carrito[i].nombreUser;
										produc=carrito[i].nombre;
										disminuirInvPubl(usuario,produc);
									}




									cout<<"FELICIDADES, SU COMPRA HA SIDO EXITOSA."<<endl;
									salir=true;
								}
							}
							if(salir==false){
								cargandoFail();
								archivo.close();
								char es;
								cout<<"�Desea cancelar la compra? (S/N)"<<endl;
								cin>>es;
								if(es=='s'){
									salir=true;
								}
							}
						}while(salir==false);
					}
				}


		}
	}while(d==2);

	char m;
	do{
		cout<<"Presione x para volver"<<endl;
		cin>>m;
		if(m=='x'){
			IngresoComprador(nombredeusuario);
		}
	}while(m!='x');
}

