/*
 * utilitarios.cpp
 *
 *  Created on: 5 jun. 2018
 *      Author: jano_
 */

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <windows.h>
#include <stdio.h>
#include <ctime>

using namespace std;


stringstream ss;

vector <string> separacion(string cadena, char caracterSeparacion){
	vector <string> comprador;
	string linea;
	ss.clear();
	ss<<cadena;
	while(getline(ss,linea,caracterSeparacion)){
		comprador.push_back(linea);
	}
	/*Prueba --> borrar comentario
	for(int i=0;i<alumno.size();i++){
		cout<<alumno[i]<<endl;
	}
	*/
	return comprador;
}



void separacionYmuestraDePublicacion(string cadena,char caracterSeparacion){

	int b,l;
	string cad;

	for(int i=0;i<8;i++){
		b=cadena.find(caracterSeparacion);
		cad=cadena.substr(0,b);
		l= cadena.length();
		cadena = cadena.substr(b+1,l);
		if(i==1){
			cout<<"Nombre del producto: "<<cad<<endl;
		}
		if(i==2){
			cout<<"Nombre del vendedor: "<<cad<<endl;
		}
		if(i==3){
			cout<<"Fecha de publicacion: "<<cad<<endl;
		}
		if(i==4){
			cout<<"Estado: "<<cad<<endl;
		}
		if(i==5){
			cout<<"Precio: "<<cad<<endl;
		}
		if(i==6){
			cout<<"Cantidad disponible: "<<cad<<endl;
		}
		if(i==7){
			cout<<"Descripcion del producto: "<<cad<<endl;
		}
	}
}

void separacionYmuestraDeInventario(string cadena,char caracterSeparacion){

	int b,l;
	string cad;

	for(int i=0;i<4;i++){
		b=cadena.find(caracterSeparacion);
		cad=cadena.substr(0,b);
		l= cadena.length();
		cadena = cadena.substr(b+1,l);
		if(i==0){
			cout<<"Nombre del producto: "<<cad<<endl;
		}
		if(i==1){
			cout<<"Fecha de Publicacion: "<<cad<<endl;
		}
		if(i==2){
			cout<<"Precio: "<<cad<<endl;
		}
		if(i==3){
			cout<<"Cantidad disponible: "<<cad<<endl<<endl;
		}

	}
}

void separacionYmuestraDeEntregas(string cadena,char caracterSeparacion){

	int b,l;
	string cad;

	for(int i=0;i<3;i++){
		b=cadena.find(caracterSeparacion);
		cad=cadena.substr(0,b);
		l= cadena.length();
		cadena = cadena.substr(b+1,l);
		if(i==0){
			cout<<"Nombre del producto: "<<cad<<endl;
		}
		if(i==1){
			cout<<"Nombre del usuario: "<<cad<<endl;
		}
		if(i==2){
			cout<<"Direccion: "<<cad<<endl;
		}

	}
}



void separacionYmuestraDePedidos(string cadena,char caracterSeparacion){

	int b,l;
	string cad;

	for(int i=0;i<5;i++){
		b=cadena.find(caracterSeparacion);
		cad=cadena.substr(0,b);
		l= cadena.length();
		cadena = cadena.substr(b+1,l);
		if(i==0){
			cout<<"Nombre del producto: "<<cad<<endl;
		}
		if(i==1){
			cout<<"Nombre del vendedor: "<<cad<<endl;
		}
		if(i==2){
			cout<<"Fecha de compra: "<<cad<<endl;
		}
		if(i==3){
			cout<<"Estado: "<<cad<<endl;
		}
		if(i==4){
			cout<<"Precio: "<<cad<<endl;
		}

	}
}

string obtenerfecha(){
	time_t now = time(0);
	tm * time = localtime(&now);
	vector<string> mes;
	mes.push_back("Enero");
	mes.push_back("Febrero");
	mes.push_back("Marzo");
	mes.push_back("Abril");
	mes.push_back("Mayo");
	mes.push_back("Junio");
	mes.push_back("Julio");
	mes.push_back("Agosto");
	mes.push_back("Septiembre");
	mes.push_back("Octubre");
	mes.push_back("Noviembre");
	mes.push_back("Diciembre");

	int year = 1900 + time->tm_year;
	stringstream fecha;
	fecha<< time->tm_mday << " de " << mes[time->tm_mon] << " del " << year<<"; "<< time->tm_hour << ":" << time->tm_min << ":" << time->tm_sec;
	return fecha.str();
}



string codigoCesar(string contrasena,int llave){
	int l, cont;
	l=contrasena.length();
	for(int i=0;i<l;i++){
		cont=0;
		while(cont!=llave){
			if((contrasena[i]>='a'&&contrasena[i]<='z')||(contrasena[i]>='A'&&contrasena[i]<='Z')){
				if(contrasena[i]=='z'){
					contrasena[i]='`'+1;
				}else if(contrasena[i]=='Z'){
					contrasena[i]='@'+1;
				}else{
					contrasena[i]++;
				}
			}
			cont++;
		}
	}
	return contrasena;
}


float convertirAreal(string dato){
	float numreal;
	ss.clear();
	ss<<dato;
	ss>>numreal;
	return numreal;
}

int convertirAentero(string dato){
	int nument=convertirAreal(dato);
	return nument;
}

int cargando() {
	int segundos=5;

	for(int i=0;i<=79;i++){
		Sleep(segundos*85/80);
	}
	cout<<"Verificacion exitosa!"<<endl;
	return 0;
}

int cargandoFail() {
	int segundos=5;

	for(int i=0;i<=79;i++){
		Sleep(segundos*85/80);
	}
	cout<<"Numero de tarjeta no existente!"<<endl;
	return 0;
}
