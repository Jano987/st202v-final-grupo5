/*
 * utilitarios.hpp
 *
 *  Created on: 5 jun. 2018
 *      Author: jano_
 */
#ifndef UTILITARIOS_UTILITARIOS_HPP_
#define UTILITARIOS_UTILITARIOS_HPP_

#include <iostream>
#include <vector>
using namespace std;

vector <string> separacion(string cadena, char caracterSeparacion);
void separacionYmuestraDePublicacion(string cadena,char caracterSeparacion);
void separacionYmuestraDeInventario(string cadena,char caracterSeparacion);
void separacionYmuestraDePedidos(string cadena,char caracterSeparacion);
void separacionYmuestraDeEntregas(string cadena,char caracterSeparacion);
float convertirAreal(string dato);
int convertirAentero(string dato);
string codigoCesar(string contrasena,int llave);
void cargando();
void cargandoFail();
string obtenerfecha();


#endif /* UTILITARIOS_UTILITARIOS_HPP_ */
