/*
 * bienvenida.cpp
 *
 *  Created on: 22 may. 2018
 *      Author: jano_
 */

#include <iostream>
#include <stdlib.h>
#include "../crearUsuario/crearUsuario.hpp"
#include "../inicioSesion/inicioSesion.hpp"
#include"../ofertas/ofertas.hpp"
using namespace std;

void Menu_Principal();

void Bienvenida(){
	string tecla;
	cout<<"================================================"<<endl;
	cout<< "\n\t*Bienvenido a EduShop* "<<endl;
	cout<< "\n\t\t\t~ Donde encontraras "<<endl;
	cout<< "\t\t\tlos precios mas "<<endl;
	cout<< "\t\t\tbajos para tu "<<endl;
	cout<< "\t\t\tregreso a clases :)"<<endl;
	cout<<endl;
	cout<<endl;
	cout<<"\t�Presione X para continuar ^-^"<<endl;
	cout<<endl;
	cout<<"================================================"<<endl;
	getline(cin,tecla);
	if(tecla=="x"){
		system("cls");
		Menu_Principal();
	}
}

void Menu_Principal(){
	int opmenu;
	cout<<"=================================="<<endl;
	cout<< "\n\t~ MENU PRINCIPAL ~ "<<endl;
	cout<<endl;
	cout<< "<1> Inicia Sesion"<< endl;
	cout<<endl;
	cout<< "    �Aun no tienes una cuenta? "<< endl;
	cout<< "    �Unete a nuestra gran comunidad de usuarios!"<<endl;
	cout<< "<2> Registrate"<<endl;
	cout<<endl;
	cout<< "<3> Ver ofertas"<<endl;
	cout<<endl;
	cout<<endl;
	cout<< "<0> Salir"<<endl;
	cout<<"==================================="<<endl;
	cin>>opmenu;
	if(opmenu==1){
		Inicio_Sesion();
	}
	if(opmenu==2){
		EleccionUsuario();
	}
	if(opmenu==0){
		exit(1);
	}
	if(opmenu==3){
			ofertas();
		}
}

