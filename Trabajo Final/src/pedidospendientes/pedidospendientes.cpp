/*
 * pedidospendientes.cpp
 *
 *  Created on: 6 jun. 2018
 *      Author: Jose
 */
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include "../ingreso/ingreso.hpp"
#include "../utilitarios/utilitarios.hpp"
using namespace std;

struct pedido{
	string nombre;
	string vendedor;
	string fecha;
	int estado;
	float precio;


	void inicializar(vector<string>datos){
		nombre=datos[0];
		vendedor=datos[1];
		fecha=datos[2];
		estado=convertirAentero(datos[3]);
		precio=convertirAreal(datos[4]);

	}
};

struct entrega{
	string nombre;
	string comprador;
	string direccion;

	void inicializar(vector<string>datos){
		nombre=datos[0];
		comprador=datos[1];
		direccion=datos[2];
	}

};

void pedidos(string nombredeusuario){
	string ruta,ruta1,ruta2;
	string linea;
	string nombre,nombre1,nombre2,nombre3,nombre4;

	ruta="Archivos/Comprador/Pedido/Pedidos_";
	nombre=ruta.append(nombredeusuario).append(".txt");

	vector<pedido> ped;
	pedido otherPed;
	vector<string> datos;
	int i=1;
	ifstream archivo;
	archivo.open(nombre.c_str());
	while(getline(archivo,linea)){
		datos=separacion(linea,'|');
		otherPed.inicializar(datos);
		ped.push_back(otherPed);
		cout<<"Numero de pedido: "<<i<<endl;
		separacionYmuestraDePedidos(linea,'|');
		i++;
	}
	archivo.close();
	cout<<"<1> Volver."<<endl;
	cout<<"<2> Confirmar pedido entregado."<<endl;
	int op;
	cin>>op;
	if(op==1){
		IngresoComprador(nombredeusuario);
	}
	if(op==2){
		int num;
		cout<<"Inserte el numero de pedido que desee confirmar: ";
		cin>>num;

		ofstream archivo1;
		//Antes de eliminarlo del vector se utiliza para agregarlo al historial
		ruta1="Archivos/Comprador/Historial/Historial_";
		nombre1=ruta1.append(nombredeusuario).append(".txt");
		archivo1.open(nombre1.c_str(),ios::app);
		archivo1<<ped[num-1].nombre<<'|'<<ped[num-1].vendedor<<'|'<<ped[num-1].fecha<<'|'<<ped[num-1].estado<<'|'<<ped[num-1].precio<<'|'<<endl;
		archivo1.close();

		ifstream archivo2;
		vector<entrega> E;
		entrega otherE;
		vector<string> datos1;
		string vendedor1=ped[num-1].vendedor;
		ruta2="Archivos/Vendedor/Entrega/Entregas_";
		nombre2=ruta2.append(vendedor1).append(".txt");
		archivo2.open(nombre2.c_str());
		while(getline(archivo2,linea)){
			datos1=separacion(linea,'|');
			otherE.inicializar(datos1);
			E.push_back(otherE);
		}
		archivo2.close();
		int con=0;
		bool seguir=true;
		for(int i=0;i<E.size();i++){
			if(E[i].nombre==ped[num-1].nombre){
				seguir=false;
			}
			if(seguir==true){
				con++;
			}
		}

		//Ahora eliminar lo de entrega
		E.erase(E.begin()+con);

		nombre3=ruta2.append(vendedor1).append(".txt");
		archivo1.open(nombre3.c_str(),ios::trunc);
		for(int i=0;i<E.size();i++){
			archivo1<<E[i].nombre<<'|'<<E[i].comprador<<'|'<<E[i].direccion<<endl;
		}
		archivo1.close();

		//Ahora si recien se elimina el Pedido Pendiente
		ped.erase(ped.begin()+num-1);


		nombre4=ruta.append(nombredeusuario).append(".txt");
		archivo1.open(nombre4.c_str());
		for(int i=0;i<ped.size();i++){
			archivo1<<ped[i].nombre<<'|'<<ped[i].vendedor<<'|'<<ped[i].fecha<<'|'<<ped[i].estado<<'|'<<ped[i].precio<<'|'<<endl;
		}
		archivo1.close();
		cout<<"Listo!"<<endl<<endl;

		char salir;
		do{
			cout<<"Presione x para volver"<<endl;
			cin>>salir;
			if(salir=='x'){
				IngresoComprador(nombredeusuario);
			}
		}while(salir!='x');

	}


}


