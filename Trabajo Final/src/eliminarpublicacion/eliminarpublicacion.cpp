/*
 * eliminarpublicacion.cpp
 *
 *  Created on: 30 may. 2018
 *      Author: Jose
 */
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <stdio.h>
#include "../ingreso/ingreso.hpp"
using namespace std;

void eliminarpublicacion(string nombreDeUsuario, string nombreDePublicacion){


	vector<string> auxI;
	vector<string> auxP;

	ifstream archivo;
	string linea,ruta1,nombre;

	ruta1="Archivos/Vendedor/Inventario/Inventario_";
	nombre=ruta1.append(nombreDeUsuario).append(".txt");
	archivo.open(nombre.c_str());

	bool salir=false;
	int ubicacionPubli=0;
	if(archivo.is_open()){
		while(getline(archivo,linea)){
			if(linea.find(nombreDePublicacion)!=string::npos){
				salir=true;
			}
			if(salir==false){
				ubicacionPubli++;
			}
			auxI.push_back(linea);
		}
	}

	archivo.close();

	string op;
	ofstream archivo1;
	if(salir==true){
		string ruta1, nombre;
		ruta1="Archivos/Vendedor/Inventario/Inventario_";
		nombre=ruta1.append(nombreDeUsuario).append(".txt");
		auxI.erase(auxI.begin()+ubicacionPubli);
		archivo1.open(nombre.c_str(),ios::trunc);
		for(int i=0;i<auxI.size();i++){
			archivo1<<auxI[i]<<endl;
		}
		archivo1.close();
		cout<<"Publicacion eliminada del inventario"<<endl;

	}else{
		cout<<"No se encontro en su inventario."<<endl;

	}


	archivo.open("Archivos/Publicaciones.txt");

	salir=false;
	ubicacionPubli=0;

	if(archivo.is_open()){
		while(getline(archivo,linea)){
			if(linea.find(nombreDePublicacion)!=string::npos){
				salir=true;
			}
			if(salir==false){
				ubicacionPubli++;
			}
			auxP.push_back(linea);
		}
	}
	archivo.close();

	if(salir==true){
		auxP.erase(auxP.begin()+ubicacionPubli);
		archivo1.open("Archivos/Publicaciones.txt",ios::trunc);
		for(int i=0;i<auxP.size();i++){
			archivo1<<auxP[i]<<endl;
			cout<<"Publicacion eliminada en Publicaciones"<<endl;
			cout<<"Presione x para volver"<<endl;

			fflush(stdout);
			fflush(stdin);
			getline(cin,op);
			if(op=="x"){
				IngresoVendedor(nombreDeUsuario);
			}
		}
		archivo1.close();
	}else{
		cout<<"No se encontro en Publicaciones."<<endl;
		cout<<"Presione x para volver"<<endl;

		fflush(stdout);
		fflush(stdin);
		getline(cin,op);
		if(op=="x"){
			IngresoVendedor(nombreDeUsuario);
		}
	}

}

